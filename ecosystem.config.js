module.exports = {
  apps : [{
    name: 'TimeOverlay',
    script: 'index.js',
    exec_mode: 'cluster',
    autorestart: true,
    instances: -1,
    log_date_format: 'YYYY-MM-DD HH:mm:ss',
    merge_logs: true,
    max_memory_restart: '1G',
  }]
};

//Modules
const http = require('http');
const fs = require('fs');

//My modules
const utils = require('./modules/utils');
const sse = require('./modules/sse');
const rscr = require('./modules/resourcer');
const rtr = require('./modules/router');

//Startup
process.on('uncaughtException', onError);

const server = http.createServer();
const SSEProvider = new sse(server);
const router = new rtr(); //Do not use server argument while using custom request processing!
const resourcer = new rscr('resources');

SSEProvider.setupHandler('/time');

var lastSecs;

function timeUpdate() {
    const now = new Date();
    if (now.getSeconds() !== lastSecs) {
        lastSecs = now.getSeconds();

        const colors = ['white', 'yellow', 'orange', 'deeppink', 'red', 'blue', 'purple', 'green', 'chocolate', 'whitesmoke'];

        SSEProvider.raiseEvent('/time', null, {
            timestamp: now.getTime(),
            digitColor: colors[now.getSeconds() % 10],
            dateColor: 'gold'
        });
    }
}

setInterval(timeUpdate, 100);

function provideFileContents(resource, type, res) {
    resourcer.get(resource, (err, contents, fromCache) => {
        if (contents) {
            res.writeHead(200, {
                'Content-Type': type + ';charset=utf-8'
            });
            res.write(contents);
            res.end();
        } else {
            res.writeHead(500, {
                'Content-Type': 'text/html;charset=utf-8'
            });
            res.write('<h1>Server Error</h1>');
            res.write('<pre>' + JSON.stringify(err) + '</pre>');
            res.end();
        }
    });
}

router.defaultRoute((req, res) => {
    res.writeHead(404, {
        'Content-Type': 'text/html;charset=utf-8'
    });
    res.end('<h1>Page not found!</h1>');
});

router.createRoute('/', (req, res) => {
    provideFileContents('pages/time.html', 'text/html', res);
});

router.createRoute('/editor', (req, res) => {
    provideFileContents('pages/hit-test.html', 'text/html', res);
});

router.createRoute('/test', (req, res) => {
    provideFileContents('pages/scaling.html', 'text/html', res);
});

router.createRoute('/resources/paper.js', (req, res) => {
    provideFileContents('scripts/paper.js', 'application/javascript', res);
});

router.createRoute('/resources/scripts/hit-test.js', (req, res) => {
    provideFileContents('page-scripts/hit-test.js', 'application/javascript', res);
});

router.createRoute('/resources/scripts/scaling.js', (req, res) => {
    provideFileContents('page-scripts/scaling.js', 'application/javascript', res);
});

router.createRoute('/resources/scripts/time.js', (req, res) => {
    provideFileContents('page-scripts/time.js', 'application/javascript', res);
});

router.createRoute('/resources/moment.js', (req, res) => {
    provideFileContents('scripts/moment-with-locales.js', 'application/javascript', res);
});

router.createRoute('/resources/eventsource.js', (req, res) => {
    provideFileContents('scripts/eventsource.min.js', 'application/javascript', res);
});

SSEProvider.on('request', (req, res, url, err) => {
    console.log('REQUEST! (' + url.pathname + ')');

    router.route(req, res);
});

server.on('clientError', (err, socket) => {
    //Throwing: RangeError: Maximum call stack size exceeded
    //https://github.com/nodejs/node/issues/7126 > v6 is not affected!

    logMessage('clientError event, processing...', 2, () => {
        socket.end('HTTP/1.1 400 Bad Request\r\n\r\n');
    });
});

server.listen(8000, () => {
    logMessage('Server startup - listening...', 2);
});

//Modes: 1 = save to file, 0 = just log to console, 2 = both
function logMessage(message, mode, callback) {
    mode = mode || 1;
    mode = (mode > 2 || mode < 0 ? 1 : mode);

    var actDate = (new Date()).toString();

    if (mode == 2 || mode == 0) {
        console.log('[%s] %s', actDate, message);

        if (mode == 0 && callback)
            callback();
    }

    if (mode == 2 || mode == 1)
        fs.appendFile('debug.log', `${actDate}\n${message}\n\n`, (writeErr) => {
            if (writeErr)
                console.log(`Caught exception: ${writeErr.stack}`);

            if (callback)
                callback();
        });
}

function onError(err) {
    console.log('[WARNING] UNCAUGHT EXCEPTION');

    logMessage(`Uncaught exception: ${err.stack}`, 1, () => {
        process.exit(1);
    });
}

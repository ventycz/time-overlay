//Modules
const fs = require('fs');

//My modules
const utils = require('./utils');

function Resourcer(resourcesFolder) {
    if (!resourcesFolder)
        return;

    var folder = utils.trimEnd(resourcesFolder, '/');
    var cachedResources = [];

    function getResourcePath(resource) {
        return folder + '/' + utils.trim(resource, '/');
    }

    function getResourceStats(resource, callback) {
        fs.stat(getResourcePath(resource), (err, stats) => {
            callback(err, stats);
        });
    }

    function getResourceContents(resource, callback) {
        fs.readFile(getResourcePath(resource), (err, data) => {
            callback(err, data);
        });
    }

    function loadResourceToCache(resource, callback) {
        getResourceContents(resource, (err, contents) => {
            if (err)
                callback(err);

            if (!err) {
                cachedResources[resource] = {
                    loaded: new Date(),
                    contents: contents
                };

                callback(err, contents);
            }
        });
    }

    //callback - latest error, contents (from file || from cache || null = absolute failure), is from cache ? || null = absolute failure
    function getResource(resource, callback) {
        if (cache = cachedResources[resource]) {
            getResourceStats(resource, (err, stats) => {
                const cached = cache.contents;

                if (!err) {
                    const loaded = cache.loaded.getTime();
                    const lastModif = stats.mtime.getTime();

                    if (loaded < lastModif) {
                        //Expired, reload!
                        loadResourceToCache(resource, (err, contents) => {
                            if (!err)
                                callback(null, contents, false);

                            //Failure, go for the cache!
                            if (err)
                                callback(err, cached, true);
                        });
                    } else {
                        //Latest!
                        callback(null, cached, true);
                    }
                }

                if (err) {
                    //Failure, go for the cache!
                    callback(err, cached, true);
                }
            });
        } else {
            //Not cached, load!
			loadResourceToCache(resource, (err, contents) => {
				if (!err)
					callback(null, contents, false);

				//ABSOLUTE FAILURE!
				if (err)
					callback(err);
			});
        }
    }

    this.get = (resource, callback) => {
        if (!resource || !callback)
            return;

        getResource(utils.trim(resource, '/'), callback);
    };
}

module.exports = Resourcer;

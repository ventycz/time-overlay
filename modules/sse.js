//Modules
const util = require('util');
const events = require('events');

//My modules
const utils = require('./utils');

function SSE(server) {
    if (!server)
        return;

    events.call(this);
    const self = this;

    //Index = path, item = handler object
    var handlers = [];
    //Index = path, item = array of listeners
    var listeners = [];

    server.on('request', onRequest);

    function onRequest(request, response) {
        const url = utils.parseUrl(request.url);
        const path = url.pathname;

        if (handlers[path]) {
            var newListener = {
                request: request,
                response: response
            }

            //Add to list of listeners on specified path
            utils.appendToNamedList(listeners, path, newListener);

            //And remove it on the listener disconnect
            function reqEnd() {
                utils.removeFromNamedList(listeners, path, newListener);
            }

            //Initialize SSE
            response.writeHead(200, {
                'Content-Type': 'text/event-stream;charset=utf-8',
                'Cache-Control': 'no-cache'
            });
            request.on('close', reqEnd);
            request.on('end', reqEnd);
        } else {
            self.emit('request', request, response, url, null);
        }
    }

    function formatAsEvent(eventName, data) {
        var result = '';

        if (eventName)
            result += 'event: ' + eventName + '\n';

        const dataLines = utils.splitLines(data);
        for (var i = 0; i < dataLines.length; i++) {
            result += 'data: ' + dataLines[i] + '\n';
        }

        if (result.length > 0)
            result += '\n';

        return result;
    }

    function handleEvent(req, res, eventName, data, next) {
        if (typeof data === 'object')
            data = JSON.stringify(data);

        res.write(formatAsEvent(eventName, data));
        next();
    }

    this.setupHandler = (path, callback) => {
        if (!path)
            return;

        handlers[path] = {
            handler: callback || null
        };
    };

    this.raiseEvent = (path, eventName, data) => {
        //Does the handler exists & has listeners ?
        if ((handler = handlers[path]) && (handlerListeners = listeners[path])) {
            utils.iterateCollection(handlerListeners, (listener, continueIt) => {
                if (handler.handler == null) {
                    handleEvent(listener.request, listener.response, eventName, data, continueIt);
                } else {
                    handler.handler(listener.request, listener.response, eventName, data, continueIt);
                }
            }, (err) => {
                if (err)
                    console.log('Failed to raise an event!');
            });
        }
    }
}

util.inherits(SSE, events);
module.exports = SSE;

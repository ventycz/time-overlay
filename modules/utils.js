const async = require('async');
const lodash = require('lodash');

//Url parsing
const parseUrl = require('url').parse;

function Util() {
    const self = this;

    this.appendToNamedList = (list, name, object) => {
        if (list[name]) {
            list[name].push(object);
        } else {
            list[name] = [object];
        }
    };

    this.removeFromNamedList = (list, name, object) => {
        if (list[name]) {
            if ((i = list[name].indexOf(object)) > -1) {
                list[name].splice(i, 1);
            }
        }
    };

    this.iterateCollection = (coll, itemCallaback, completeCallback) => {
        if (!coll || !itemCallaback || !completeCallback)
            return;

        async.each(coll, function(item, continueIt) {
            itemCallaback(item, continueIt);
        }, function(err) {
            completeCallback(err);
        });
    };

    this.splitLines = (str) => {
        if (str.split)
            return str.split(/\r?\n/);

        return [str];
    };

    this.parseUrl = (url) => {
        if (!url)
            return;

        var parsed = parseUrl(url);
        parsed.pathname = lodash.trimEnd(parsed.pathname, '/');
        parsed.pathname = parsed.pathname.length > 0 ? parsed.pathname : '/';

        return parsed;
    };

    this.trim = lodash.trim;
    this.trimStart = lodash.trimStart;
    this.trimEnd = lodash.trimEnd;
}

module.exports = new Util();

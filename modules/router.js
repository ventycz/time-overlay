//Modules
const async = require('async');

//My modules
const utils = require('./utils');

function router(httpServer) {
    if (httpServer)
        httpServer.on('request', onRequest);

    const self = this;

    //List of available routes
    var routes;
    var defaultRoute;

    function createRegex(path) {
        if (typeof path !== 'string')
            return;

        var gen = path.replace(/\//g, '\\/');
        return new RegExp('^' + gen + '$');
    }

    function onRequest(request, response) {
        self.route(request, response);
    }

    this.defaultRoute = (newRouteCallback) => {
        defaultRoute = newRouteCallback;
    };

    this.createRoute = (route, callback) => {
        if (!routes)
            routes = [];

        routes.push({
            route: createRegex(route),
            callback: callback
        });
    };

    this.route = (req, res) => {
        if (!req || !res)
            return;

        const url = utils.parseUrl(req.url);

        async.detectSeries(routes, (item, callback) => {
            const matches = item.route.test(url.pathname);

            callback(null, matches);
        }, (err, result) => {
            if (err)
                console.log('Route (%s) Error: %s', url.pathname, err);

            if (!err && !result)
                defaultRoute(req, res);

            if (!err && result)
                result.callback(req, res);
        });
    };
}

module.exports = router;

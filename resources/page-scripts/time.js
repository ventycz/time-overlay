function RemoveElementClass(id, cls) {
	var element = document.getElementById(id);
	if (element) {
		element.className = element.className.replace(new RegExp('\\b' + cls + '\\b'), '');
	}
}

var evtSource = new EventSource("/time");

function updateVariableContents(varName, varValue) {
	//console.log("[VALUE]" + varName + " = " + varValue);

	var element = document.getElementById("variable-" + varName);

	if (element) {
		element.innerHTML = varValue;
	} else {
		console.log("Invalid variable - " + varName);
	}
}

function updateVariableColor(varName, varColor) {
	//console.log("[COLOR]" + varName + " > " + varColor);

	var element = document.getElementById("variable-" + varName);

	if (element) {
		element.style.color = varColor;
	} else {
		console.log("Invalid variable - " + varName);
	}
}

function updateVariablePosition(varName, varX, varY) {
	var element = document.getElementById("variable-" + varName);

	if (element) {
		element.style.position = 'fixed';
		element.style.left = varX + 'px';
		element.style.top = varY + 'px';
	} else {
		console.log("Invalid variable - " + varName);
	}
}

evtSource.onmessage = function(event) {
	const data = JSON.parse(event.data);

	if (data.timestamp) {
		const date = moment(data.timestamp).locale(navigator.language || 'en');

		updateVariableContents('time', date.format('HH:mm:ss'));
		updateVariableContents('date', date.format('D. MMMM YYYY'));
	}

	if (data.digitColor)
		updateVariableColor('time', data.digitColor);

	if (data.dateColor)
		updateVariableColor('date', data.dateColor);
};

window.addEventListener("beforeunload", function (e) {
	console.log('EXIT');
	evtSource.close();
});
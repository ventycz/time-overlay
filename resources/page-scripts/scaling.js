/* http://jsfiddle.net/u1fo39d8/3/ */
function init() {
    var original_size = {
        width: 800,
        height: 600
    };

    var canvas = document.getElementById('canvas');
    paper.setup(canvas);

    var actual_ratio = canvas.clientWidth / original_size.width;
    var last_width = canvas.clientWidth;

    var raster1 = new paper.Raster('https://placehold.it/800x600');
    raster1.onLoad = function() {
        raster1.position = paper.view.center;
        raster1.size = paper.view.size;
    };
    var ball1 = new paper.Shape.Circle(new paper.Point(getReal(80), getReal(50)), getReal(30));
    ball1.fillColor = 'red';
    ball1.onMouseUp = function() {
        alert("ball 1!");
    };

    var ball2 = new paper.Shape.Circle(new paper.Point(getReal(160), getReal(100)), getReal(60));
    ball2.fillColor = 'blue';

    var ball3 = new paper.Shape.Circle(new paper.Point(getReal(400), getReal(60)), getReal(40));
    ball3.fillColor = 'yellow';

    var ball4 = new paper.Shape.Circle(new paper.Point(getReal(800), getReal(600)), getReal(25));
    ball4.fillColor = 'green';

    function calc() {
        paper.view.setViewSize(new paper.Size(getReal(800), getReal(600)));
        var scale = getScale();
        ball1.position = new paper.Point(getReal(80), getReal(50));
        ball1.scale(scale);

        ball2.position = new paper.Point(getReal(160), getReal(100));
        ball2.scale(scale);

        ball3.position = new paper.Point(getReal(400), getReal(60));
        ball3.scale(scale);

        ball4.position = new paper.Point(getReal(800), getReal(600));
        ball4.scale(scale);

        raster1.scale(scale);
        raster1.position = paper.view.center;
    }

    paper.view.onResize = function(event) {
        calc();
    };

    function getReal(val) {
        var actual_ratio = canvas.clientWidth / original_size.width;
        return val * actual_ratio;
    }

    function getScale() {
        var scale = canvas.clientWidth / last_width;
        last_width = canvas.clientWidth;

        return scale;
    }

    calc();
}

window.addEventListener('load', init);

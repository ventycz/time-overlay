function init() {
    //PaperJS setup
    var canvas = document.getElementById('canvas');
    paper.setup(canvas);

    //Event routing
    var tool = new paper.Tool();
    tool.onMouseDown = onMouseDown;
    tool.onMouseMove = onMouseMove;
    tool.onMouseDrag = onMouseDrag;

    //Startup
    createPaths({
        paths: 10,
        minPoints: 3,
        maxPoints: 5,
        minRadius: 30,
        maxRadius: 90
    });

    function createPaths(options) {
        var radiusDelta = options.maxRadius - options.minRadius;
        var pointsDelta = options.maxPoints - options.minPoints;

        for (var i = 0; i < options.paths; i++) {
            var radius = options.minRadius + Math.random() * radiusDelta;
            var points = options.minPoints + Math.floor(Math.random() * pointsDelta);
            var center = paper.Point.random().multiply(paper.view.size);

            var path = createBlob(center, radius, points, {
                lightness: (Math.random() - 0.5) * 0.4 + 0.4,
                hue: Math.random() * 360,
                saturation: 1
            }, 'black');
        };
    }

    function createBlob(center, maxRadius, points, fill, stroke) {
        var path = new paper.Path();
        path.closed = true;

        if (fill)
            path.fillColor = fill;
        if (stroke)
            path.strokeColor = stroke;

        for (var i = 0; i < points; i++) {
            var delta = new paper.Point({
                length: (maxRadius * 0.5) + (Math.random() * maxRadius * 0.5),
                angle: (360 / points) * i
            });

            path.add(center.add(delta));
        }
        //path.smooth();
        return path;
    }

    //Stores selected segment (point) & path
    var segment, path;

    function onMouseDown(event) {
        segment = path = null;

        var hitResult = paper.project.hitTest(event.point, {
            segments: true,
            stroke: true,
            fill: true,
            tolerance: 5
        });

        if (hitResult) {
            if (event.modifiers.shift) {
                if (hitResult.type == 'segment') {
                    //Remove selected point on active blob
                    hitResult.segment.remove();
                }
            } else {
                path = hitResult.item;

                if (hitResult.type == 'segment') {
                    segment = hitResult.segment;
                } else if (hitResult.type == 'stroke') {
                    segment = path.insert(hitResult.location.index + 1, event.point);
                    //path.smooth();
                } else if (hitResult.type == 'fill') {
                    //Tap on blob
                    //Brings to foreground somewhat :3
                    paper.project.activeLayer.addChild(hitResult.item);
                }
            }
        }
    }

    var hoveredItem;

    function onMouseMove(event) {
        //paper.project.activeLayer.selected = false;

        //Highlight the hovered item
        if (event.item) {
            if (hoveredItem)
                hoveredItem.selected = false;

            hoveredItem = event.item;
            hoveredItem.selected = true;
        } else {
            if (hoveredItem)
                hoveredItem.selected = false;

            hoveredItem = null;
        }
    }

    function onMouseDrag(event) {
        const useMousePos = false;

        if (segment) {
            //Moving the blob's point
            const newPos = (useMousePos ? event.point : segment.point.add(event.delta));
            const viewSize = paper.view.size;

            var check_X = (newPos.x > 0 && newPos.x < viewSize.width);
            var check_Y = (newPos.y > 0 && newPos.y < viewSize.height);

            if (check_X)
                segment.point.x = newPos.x;

            if (check_Y)
                segment.point.y = newPos.y;
        } else if (path) {
            //Moving the blob
            //Note: position is related to the center of the blob
            const newPos = (useMousePos ? event.point : path.position.add(event.delta))
            const viewSize = paper.view.size;
            const bounds = path.strokeBounds;

            var check_X = ((newPos.x - bounds.width / 2) > 0 && (newPos.x + bounds.width / 2) < viewSize.width);
            var check_Y = ((newPos.y - bounds.height / 2) > 0 && (newPos.y + bounds.height / 2) < viewSize.height);

            if (check_X)
                path.position.x = newPos.x;

            if (check_Y)
                path.position.y = newPos.y;
        }
    }
}

window.addEventListener('load', init);
